package Ex1;

public class PaymentProcessor {
    private CarteCreditManager carteCreditManager;
    private PaymentGateway paymentGateway;

    public PaymentProcessor() {
        this.carteCreditManager = new CarteCreditManager();
        this.paymentGateway = new PaymentGateway();
    }

    public String processPayment(double montant, String numCarte, String cvv) {
        boolean carteValide = carteCreditManager.verifyCard(numCarte, cvv);
        if (carteValide) {
            return paymentGateway.makePayment(montant);
        } else {
            return null;
        }
    }
}
