package Ex2;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Medecin {
    private int id;
    private List<LocalDateTime> rendezVous;

    public Medecin(int id) {
        this.id = id;
        rendezVous = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public boolean estDisponible(LocalDateTime dateRendezVous) {
        LocalDateTime dateRendezVousSansHeure = dateRendezVous.withHour(0).withMinute(0).withSecond(0).withNano(0);
        int rendezVousDuJour = getNombreRendezVous(dateRendezVousSansHeure);
        return rendezVousDuJour < 4;
    }


    public void ajouterRendezVous(LocalDateTime dateRendezVous) {
        rendezVous.add(dateRendezVous);
    }

    public int getNombreRendezVous(LocalDateTime dateRendezVous) {
        int count = 0;
        LocalDateTime dateRendezVousSansHeure = dateRendezVous.withHour(0).withMinute(0).withSecond(0).withNano(0);
        for (LocalDateTime rdv : rendezVous) {
            LocalDateTime rdvSansHeure = rdv.withHour(0).withMinute(0).withSecond(0).withNano(0);
            if (rdvSansHeure.equals(dateRendezVousSansHeure)) {
                count++;
            }
        }
        return count;
    }
}