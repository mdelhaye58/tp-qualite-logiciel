package Ex2;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class GestionRendezVous {
    private Map<Integer, Patient> patients;
    private Map<Integer, Medecin> medecins;
    private Map<LocalDateTime, Integer> rendezVousParDate;

    public GestionRendezVous() {
        patients = new HashMap<>();
        medecins = new HashMap<>();
        rendezVousParDate = new HashMap<>();
    }

    public void ajouterPatient(Patient patient) {
        patients.put(patient.getId(), patient);
    }

    public void ajouterMedecin(Medecin medecin) {
        medecins.put(medecin.getId(), medecin);
    }

    public boolean reserverRendezVous(int patientId, int medecinId, LocalDateTime dateRendezVous) {
        Patient patient = patients.get(patientId);
        Medecin medecin = medecins.get(medecinId);

        if (patient == null || medecin == null) {
            return false;
        }

        if (!medecin.estDisponible(dateRendezVous)) {
            return false;
        }

        if (rendezVousParDate.containsKey(dateRendezVous)) {
            return false;
        }

        LocalDateTime dateRendezVousSansHeure = dateRendezVous.withHour(0).withMinute(0).withSecond(0).withNano(0);
        if (medecin.getNombreRendezVous(dateRendezVousSansHeure) >= 3) {
            return false;
        }

        medecin.ajouterRendezVous(dateRendezVous);
        rendezVousParDate.put(dateRendezVous, patientId);
        return true;
    }


}