package Ex2;

public class Patient {
    private int id;

    public Patient(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}