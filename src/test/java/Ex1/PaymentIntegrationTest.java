package Ex1;
import org.junit.Assert;
import org.junit.Test;

public class PaymentIntegrationTest {
    PaymentProcessor paymentProcessor = new PaymentProcessor();
    CarteCreditManager carteCreditManager = new CarteCreditManager();
    PaymentGateway paymentGateway = new PaymentGateway();
	
    @Test
    public void testCreditCardAuthFail() {
        String cardNum = "1212343456567878";
        String cvv = "99";

        boolean validCard = carteCreditManager.verifyCard(cardNum, cvv);
        Assert.assertFalse("La carte de crédit est invalide.", validCard);
    }
   
    @Test
    public void testPayementPass() {
        String cardNum = "1212343456567878";
        String cvv = "999";
        double amount = 50;

        boolean validCard = carteCreditManager.verifyCard(cardNum, cvv);
        Assert.assertTrue("La carte de crédit est valide.", validCard);

        String transactionId = paymentProcessor.processPayment(amount, cardNum, cvv);
        Assert.assertNotNull("L'identifiant de transaction est renvoyé.", transactionId);

        boolean paymentAccepted = paymentGateway.verifyPayment(transactionId);
        Assert.assertTrue("Le paiement est accepté par le fournisseur de services de paiement.", paymentAccepted);
    }
    
    @Test
    public void testPayementNotPass() {
        String cardNum = "1212343456567878";
        String cvv = "999";
        double amount = 50;

        boolean validCard = carteCreditManager.verifyCard(cardNum, cvv);
        Assert.assertTrue("La carte de crédit est valide.", validCard);

        String transactionId = paymentProcessor.processPayment(amount, cardNum, cvv);
        Assert.assertNotNull("L'identifiant de transaction est renvoyé.", transactionId);

        boolean paymentAccepted = paymentGateway.verifyPaymentFail(transactionId);
        Assert.assertFalse("Le paiement n'est pas accepté par le fournisseur de services de paiement.", paymentAccepted);
    }
}
