package Ex2;
import org.junit.Test;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

public class ReservationTest {

    @Test
    public void testReservationValide() {
        GestionRendezVous gestionRendezVous = new GestionRendezVous();

        gestionRendezVous.ajouterPatient(new Patient(1));
        gestionRendezVous.ajouterMedecin(new Medecin(1));

        LocalDateTime dateRendezVous = LocalDateTime.of(2022, 4, 1, 10, 0);
        boolean reservationEffectuee = gestionRendezVous.reserverRendezVous(1, 1, dateRendezVous);

        assertTrue(reservationEffectuee);
    }

    @Test
    public void testReservationPatientInvalide() {
        GestionRendezVous gestionRendezVous = new GestionRendezVous();
        gestionRendezVous.ajouterMedecin(new Medecin(1));

        LocalDateTime dateRendezVous = LocalDateTime.of(2022, 4, 1, 10, 0);
        boolean reservationEffectuee = gestionRendezVous.reserverRendezVous(1, 1, dateRendezVous);

        assertFalse(reservationEffectuee);
    }

    @Test
    public void testReservationMedecinInvalide() {
        GestionRendezVous gestionRendezVous = new GestionRendezVous();
        gestionRendezVous.ajouterPatient(new Patient(1));

        LocalDateTime dateRendezVous = LocalDateTime.of(2022, 4, 1, 10, 0);
        boolean reservationEffectuee = gestionRendezVous.reserverRendezVous(1, 1, dateRendezVous);

        assertFalse(reservationEffectuee);
    }

    @Test
    public void testReservationDateIndisponible() {
        GestionRendezVous gestionRendezVous = new GestionRendezVous();
        gestionRendezVous.ajouterPatient(new Patient(1));
        gestionRendezVous.ajouterMedecin(new Medecin(1));

        LocalDateTime dateRendezVous1 = LocalDateTime.of(2022, 4, 1, 10, 0);
        LocalDateTime dateRendezVous2 = LocalDateTime.of(2022, 4, 1, 11, 0);
        LocalDateTime dateRendezVous3 = LocalDateTime.of(2022, 4, 1, 12, 0);
        LocalDateTime dateRendezVous4 = LocalDateTime.of(2022, 4, 1, 13, 0);

        boolean reservationEffectuee1 = gestionRendezVous.reserverRendezVous(1, 1, dateRendezVous1);
        boolean reservationEffectuee2 = gestionRendezVous.reserverRendezVous(1, 1, dateRendezVous2);
        boolean reservationEffectuee3 = gestionRendezVous.reserverRendezVous(1, 1, dateRendezVous3);
        boolean reservationEffectuee4 = gestionRendezVous.reserverRendezVous(1, 1, dateRendezVous4);

        assertFalse(reservationEffectuee4);
    }


    @Test
    public void testReservationPatientOccupe() {
        GestionRendezVous gestionRendezVous = new GestionRendezVous();
        gestionRendezVous.ajouterPatient(new Patient(1));
        gestionRendezVous.ajouterMedecin(new Medecin(1));
        LocalDateTime dateRendezVous = LocalDateTime.of(2022, 4, 1, 10, 0);

        gestionRendezVous.reserverRendezVous(1, 1, dateRendezVous);
        boolean reservationEffectuee = gestionRendezVous.reserverRendezVous(1, 1, dateRendezVous);

        assertFalse(reservationEffectuee);
    }

    @Test
    public void testReservationMedecinOccupe() {
        GestionRendezVous gestionRendezVous = new GestionRendezVous();
        gestionRendezVous.ajouterPatient(new Patient(1));
        gestionRendezVous.ajouterMedecin(new Medecin(2));
        LocalDateTime dateRendezVous = LocalDateTime.of(2022, 4, 2, 13, 0);

        gestionRendezVous.reserverRendezVous(1, 2, dateRendezVous);
        gestionRendezVous.reserverRendezVous(2, 2, dateRendezVous);
        gestionRendezVous.reserverRendezVous(3, 2, dateRendezVous);
        gestionRendezVous.reserverRendezVous(4, 2, dateRendezVous);
        boolean reservationEffectuee = gestionRendezVous.reserverRendezVous(5, 2, dateRendezVous);

        assertFalse(reservationEffectuee);
    }

    @Test
    public void testReservationParametre() {
        GestionRendezVous gestionRendezVous = new GestionRendezVous();
        gestionRendezVous.ajouterPatient(new Patient(1));
        gestionRendezVous.ajouterPatient(new Patient(2));
        gestionRendezVous.ajouterPatient(new Patient(3));
        gestionRendezVous.ajouterPatient(new Patient(4));
        gestionRendezVous.ajouterMedecin(new Medecin(1));
        gestionRendezVous.ajouterMedecin(new Medecin(2));

        boolean expectedResult = true;

        LocalDateTime dateRendezVous1 = LocalDateTime.of(2022, 4, 1, 10, 0);
        boolean reservationEffectuee1 = gestionRendezVous.reserverRendezVous(1, 1, dateRendezVous1);
        assertEquals(expectedResult, reservationEffectuee1);

        LocalDateTime dateRendezVous2 = LocalDateTime.of(2022, 4, 1, 11, 0);
        boolean reservationEffectuee2 = gestionRendezVous.reserverRendezVous(2, 1, dateRendezVous2);
        assertEquals(expectedResult, reservationEffectuee2);

        LocalDateTime dateRendezVous3 = LocalDateTime.of(2022, 4, 2, 13, 0);
        boolean reservationEffectuee3 = gestionRendezVous.reserverRendezVous(1, 2, dateRendezVous3);
        assertEquals(expectedResult, reservationEffectuee3);

        LocalDateTime dateRendezVous4 = LocalDateTime.of(2022, 4, 2, 14, 0);
        boolean reservationEffectuee4 = gestionRendezVous.reserverRendezVous(2, 2, dateRendezVous4);
        assertEquals(expectedResult, reservationEffectuee4);

        LocalDateTime dateRendezVous5 = LocalDateTime.of(2022, 4, 2, 15, 0);
        boolean reservationEffectuee5 = gestionRendezVous.reserverRendezVous(3, 2, dateRendezVous5);
        assertEquals(expectedResult, reservationEffectuee5);

        LocalDateTime dateRendezVous6 = LocalDateTime.of(2022, 4, 2, 16, 0);
        boolean reservationEffectuee6 = gestionRendezVous.reserverRendezVous(4, 2, dateRendezVous6);
        expectedResult = false;
        assertEquals(expectedResult, reservationEffectuee6);
    }
}
